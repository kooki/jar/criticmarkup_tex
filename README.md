# criticmarkup_tex

This jar is a set of function to print CriticMarkup in a LaTeX document.

## Dependencies

In order to work this jar will need the package **color**.

```tex
\usepackage{color}
```

## CriticMarkup Memento


Addition
: `{++Addition++}`


Comment
: `{>>Comment<<}`


Deletion
: `{--Deletion--}`

Highlight
: `{==Highlight==}{>>Comment<<}`

Substitution
: `{~~Text~>Substitution~~}`

For more information go take a look to the [website](http://criticmarkup.com/).

## Extensions

**__criticmarkup_addition__**
: Print addition in green

**__criticmarkup_comment__**
: Print comment in gray

**__criticmarkup_deletion__**
: Print deletion in red

**__criticmarkup_highlight__**
: Print highlight in orange and gray

**__criticmarkup_substitution__**
: Print substitution in red and green

## Example

To properly use this jar we encourage you to create two target in your `kooki.yaml`, like the following example.

```
default:
  template: template.tex
  context: .
  jars:
    - er_report
  metadata:
    - metadata.yaml
  toppings:
    - kooki-xelatex $KOOKI_EXPORT_FILE

kooki:
  normal:
    output: report
  comments:
    output: report_comments
    jars:
      - criticmarkup_tex
```